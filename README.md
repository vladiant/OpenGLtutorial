# OpenGLtutorial

OpenGL tutorial samples based on https://github.com/VictorGordan/opengl-tutorials and [OpenGL Tutorials](https://www.youtube.com/watch?v=XpBGwZNyUh0&list=PLPaoO-vpZnumdcb4tZc4x5Q-v7CkrQ6M-) from [Victor Gordan](https://www.youtube.com/c/VictorGordan)

## Code improvement commands
```bash
cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
run-clang-tidy -checks='cppcoreguidelines-*,-cppcoreguidelines-prefer-member-initializer,readibility-*,modernize-*,-modernize-use-trailing-return-type,misc-*,clang-analyzer-*' -fix
```
